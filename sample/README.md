Setup for project's contributors
--------------------------------

1. Create virtualenv

    `virtualenv images_placeholder`

2. Go to `sample` folder(sample project used for development):

    `cd django_images_placeholder/sample`

2. Update requirementsi, install this package, apply migrations:
```
    pip install -r requirements.txt
    pip install -e ..
    python manage.py migrate
```

4. Visit the page:

    `http://127.0.0.1:8000`
