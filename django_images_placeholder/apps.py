from __future__ import unicode_literals

from django.apps import AppConfig


class DjangoImagesPlaceholderConfig(AppConfig):
    name = 'django_images_placeholder'
    verbose_name = "Django Images Placeholder"

